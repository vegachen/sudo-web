import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { Card, Row, Col, DatePicker, Select } from 'antd'
import ReactEcharts from 'echarts-for-react'
import { router } from 'utils'
import { stringify } from 'qs'
import { withI18n } from '@lingui/react'
import moment from 'moment';
const { Option } = Select;
const { RangePicker } = DatePicker; 


const SELECTABLE_COLS = ['type', 'device'];

const INVISIBLE_COLS = ['date'].concat(SELECTABLE_COLS);
@withI18n()
@connect(({ plan, loading }) => ({ plan, loading }))
class Plan extends PureComponent {
  handleDateChange = change => {
    const { pathname, query } = this.props.location
    router.push({
      pathname,
      search: stringify({
        ...query,
        startDate: change[0].format('YYYY/MM/DD'),
        endDate: change[1].format('YYYY/MM/DD'),
      })
    })
  }

  handleFilterChange = (type, value) => {
    const { pathname, query } = this.props.location
    const search = {...query}
    search[type] = value;
    router.push({
      pathname,
      search: stringify(search)
    })
  }

  render() {
    let {startDate, endDate } = this.props.location.query;
    const { list } = this.props.plan;
    if (!list) {
      return <div>loading ...</div>;
    }
    startDate = startDate ? moment(startDate) : moment().subtract(1, 'week');
    endDate = endDate ? moment(endDate) : moment();
    let filteredPlan = list.filter(d => moment(d.date).isBetween(startDate, endDate, null, '[]'));
    const selectors = SELECTABLE_COLS.map(
      selectedCol => {
        const optionsSet = new Set()
        filteredPlan.forEach(plan => optionsSet.add(plan[selectedCol]))
        const selectOptions = Array.from(optionsSet)
        return (
          <Card title='' bordered={false} className='card-item'>
              {selectedCol + ": "}
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select the plan"
                value={this.props.location.query[selectedCol]}
                onChange={v => this.handleFilterChange(selectedCol, v)}
              >
              {
                selectOptions.map(o => <Option value={o}>{o}</Option>)
              }
            </Select>
          </Card>
        )
    })
    SELECTABLE_COLS.forEach(
      col => {
        if (this.props.location.query[col]) {
          filteredPlan = filteredPlan.filter(d => d[col] == this.props.location.query[col]);
        }
      }
    )
    let cols;
    if (!!filteredPlan[0]) {
      cols = Object.keys(filteredPlan[0]).filter(k => INVISIBLE_COLS.indexOf(k)<0);
    } else {
      cols = []
    }
    
    
    const series = cols.map(col => ({
      name: col,
      type: 'line',
      // '[]' means start and end are both inclusive
      data: filteredPlan.map(d => ({
        name: d[col],
        value: [d.date, d[col]]
      })),
    }));
  
    const option = {
      title: {
        text: '投放计划',
      },
      tooltip: {
        trigger: 'axis',
      },
      legend: {
        data: cols,
      },
      toolbox: {
        feature: {
          saveAsImage: {},
        },
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true,
      },
      xAxis: [
        {
          type: 'time',
        },
      ],
      yAxis: [
        {
          type: 'value',
        },
      ],
      series
    }

    return (
      <div>
        <Row gutter={10}>
          <Col span={24}>
            <Card title='' bordered={false} className='card-item'>
              <RangePicker
                defaultValue={[moment(startDate), moment(endDate)]}
                onChange={this.handleDateChange}
              />
            </Card>
          </Col>
        </Row>

        <Row gutter={10}>
          <Col span={24}>
            {selectors}
          </Col>
        </Row>

        <Row gutter={10}>
          <Col span={24}>
            <Card title='' bordered={false} className='card-item'>
              <ReactEcharts
                option={option}
                style={{ height: '350px', width: '100%' }}
                theme="macarons"
              />
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

Plan.propTypes = {
  plan: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default Plan
