import modelExtend from 'dva-model-extend'
import api from 'api'
import { pathMatchRegexp } from 'utils'
import { model } from 'utils/model'

const { queryPlanList } = api

export default modelExtend(model, {
  namespace: 'plan',
  state: {
    list: []
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathMatchRegexp('/plan', location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryPlanList, payload)
      if (data.success) {
        yield put({
          type: 'updateState',
          payload: {
            list: data.list,
          },
        })
      } else {
        throw data
      }
    },
  },
})
