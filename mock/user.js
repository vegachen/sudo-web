import { Mock, Constant, qs, randomAvatar } from './_utils'

const { ApiPrefix } = Constant

let usersListData = Mock.mock({
  'data|80-100': [
    {
      id: '@id',
      name: '@name',
      nickName: '@last',
      phone: /^1[34578]\d{9}$/,
      'age|11-99': 1,
      address: '@county(true)',
      isMale: '@boolean',
      email: '@email',
      createTime: '@datetime',
      avatar() {
        return randomAvatar()
      },
    },
  ],
})

let database = usersListData.data

const EnumRoleType = {
  ADMIN: 'admin',
  DEFAULT: 'guest',
  DEVELOPER: 'developer',
}

const userPermission = {
  DEFAULT: {
    visit: ['1', '2', '21', '7', '5', '51', '52', '53'],
    role: EnumRoleType.DEFAULT,
  },
  ADMIN: {
    role: EnumRoleType.ADMIN,
  },
  DEVELOPER: {
    role: EnumRoleType.DEVELOPER,
  },
}


const queryArray = (array, key, keyAlias = 'key') => {
  if (!(array instanceof Array)) {
    return null
  }
  let data

  for (let item of array) {
    if (item[keyAlias] === key) {
      data = item
      break
    }
  }

  if (data) {
    return data
  }
  return null
}

const NOTFOUND = {
  message: 'Not Found',
  documentation_url: 'http://localhost:8000/request',
}

// const publicKey = '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhph8tFgJTLKk0pdiydp4\nXUzM64CrTVZJu0q0OvcZp6HBveLkSV03O1Bnx8Sqtqo/jR5a2Tr5oa6BD5VduSAF\nYJC2Nd4EJ9eCOhRf9BAulM8BMUE0tNzyNmSIchZuJfAYjESowfGCnAkz0ATxSLJN\nGnnUTFIBEEEgoAooUPXgIH8DM06wL5VKhWk+4u0eNPL6QsrcjkHLOV8HDItvUQNC\npklQDvPLxfsRzkuynxL37/gK4Hx5wQ1C4tKa+ObH0XU1iy7AsWdBUaVbS8SgPGqm\nSUEPettdq+iol+7S5mvM7PsiOWZMRoPo+DqkkS2rUWKYIBPFEfOxrkxdJhkhfl3O\nvQIDAQAB\n-----END PUBLIC KEY-----'

module.exports = {
  // [`GET ${ApiPrefix}/user/logout`](req, res) {
  //   res.clearCookie('token')
  //   res.status(200).end()
  // },

  // [`POST ${ApiPrefix}/user/login`](req, res) {
  //   const { username, password } = req.body
  //   const user = adminUsers.filter(item => item.username === username)

  //   if (user.length > 0 && user[0].password === password) {
  //     const now = new Date()
  //     now.setDate(now.getDate() + 1)
  //     res.cookie(
  //       'token',
  //       JSON.stringify({ id: user[0].id, deadline: now.getTime() }),
  //       {
  //         maxAge: 900000,
  //         httpOnly: true,
  //       }
  //     )
  //     res.json({ success: true, message: 'Ok' })
  //   } else {
  //     res.status(400).end()
  //   }
  // },

  // [`GET /auth`](req, res) {
  //   res.redirect('https://sudotech.yufuid.com/sso/ai-3bb48d0516e7451eb8aa85c4b42bd1cb')
  // },

  // [`GET /auth/sso`](req, res) {
  //   const { query } = req
  //   let token = query['id_token']
  //   try {
  //     jwt.verify(token, publicKey, { algorithm: 'RS256' })
  //     res.cookie('token', token,{
  //       maxAge: 900000,
  //       httpOnly: true,
  //     })
  //     res.redirect('/')
  //   } catch (err) {
  //     const response = {}
  //     response.success = false
  //     response.message = err
  //     res.json(response)
  //   }
  // },

  // [`GET ${ApiPrefix}/user`](req, res) {

  //   const cookie = req.headers.cookie || ''
  //   const cookies = qs.parse(cookie.replace(/\s/g, ''), { delimiter: ';' })
  //   let token = cookies.token
  //   const response = {}
  //   if (!token) {
  //     res.status(200).send({ message: 'Not Login' })
  //     return
  //   }
  //   try {
  //     const userItem = jwt.verify(token, publicKey, { algorithm: 'RS256' })
  //     response.success = true
  //     if (userItem.permissions) {
  //       userItem.permissions = JSON.parse(userItem.permissions)
  //     }
  //     response.user = userItem
  //   } catch (err) {
  //     response.success = false
  //     response.message = err
  //   }
    
  //   res.json(response)
  // },

  [`GET ${ApiPrefix}/users`](req, res) {
    const { query } = req
    let { pageSize, page, ...other } = query
    pageSize = pageSize || 10
    page = page || 1

    let newData = database
    for (let key in other) {
      if ({}.hasOwnProperty.call(other, key)) {
        newData = newData.filter(item => {
          if ({}.hasOwnProperty.call(item, key)) {
            if (key === 'address') {
              return other[key].every(iitem => item[key].indexOf(iitem) > -1)
            } else if (key === 'createTime') {
              const start = new Date(other[key][0]).getTime()
              const end = new Date(other[key][1]).getTime()
              const now = new Date(item[key]).getTime()

              if (start && end) {
                return now >= start && now <= end
              }
              return true
            }
            return (
              String(item[key])
                .trim()
                .indexOf(decodeURI(other[key]).trim()) > -1
            )
          }
          return true
        })
      }
    }

    res.status(200).json({
      data: newData.slice((page - 1) * pageSize, page * pageSize),
      total: newData.length,
    })
  },

  [`POST ${ApiPrefix}/users/delete`](req, res) {
    const { ids=[] } = req.body
    database = database.filter(item => !ids.some(_ => _ === item.id))
    res.status(204).end()
  },

  [`POST ${ApiPrefix}/user`](req, res) {
    const newData = req.body
    newData.createTime = Mock.mock('@now')
    newData.avatar =
      newData.avatar ||
      Mock.Random.image(
        '100x100',
        Mock.Random.color(),
        '#757575',
        'png',
        newData.nickName.substr(0, 1)
      )
    newData.id = Mock.mock('@id')

    database.unshift(newData)

    res.status(200).end()
  },

  [`GET ${ApiPrefix}/user/:id`](req, res) {
    const { id } = req.params
    const data = queryArray(database, id, 'id')
    if (data) {
      res.status(200).json(data)
    } else {
      res.status(200).json(NOTFOUND)
    }
  },

  [`DELETE ${ApiPrefix}/user/:id`](req, res) {
    const { id } = req.params
    const data = queryArray(database, id, 'id')
    if (data) {
      database = database.filter(item => item.id !== id)
      res.status(204).end()
    } else {
      res.status(200).json(NOTFOUND)
    }
  },

  [`PATCH ${ApiPrefix}/user/:id`](req, res) {
    const { id } = req.params
    const editItem = req.body
    let isExist = false

    database = database.map(item => {
      if (item.id === id) {
        isExist = true
        return Object.assign({}, item, editItem)
      }
      return item
    })

    if (isExist) {
      res.status(201).end()
    } else {
      res.status(200).json(NOTFOUND)
    }
  },
}
