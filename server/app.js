const express = require('express')
const path = require('path')
const jwt = require('jsonwebtoken')
const qs = require('qs')
const parse = require('csv-parse')
const fs = require('fs')

const app = express();
const port = 3002

const  ApiPrefix = '/api/v1'

const publicKey = '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhph8tFgJTLKk0pdiydp4\nXUzM64CrTVZJu0q0OvcZp6HBveLkSV03O1Bnx8Sqtqo/jR5a2Tr5oa6BD5VduSAF\nYJC2Nd4EJ9eCOhRf9BAulM8BMUE0tNzyNmSIchZuJfAYjESowfGCnAkz0ATxSLJN\nGnnUTFIBEEEgoAooUPXgIH8DM06wL5VKhWk+4u0eNPL6QsrcjkHLOV8HDItvUQNC\npklQDvPLxfsRzkuynxL37/gK4Hx5wQ1C4tKa+ObH0XU1iy7AsWdBUaVbS8SgPGqm\nSUEPettdq+iol+7S5mvM7PsiOWZMRoPo+DqkkS2rUWKYIBPFEfOxrkxdJhkhfl3O\nvQIDAQAB\n-----END PUBLIC KEY-----'

app.get(`${ApiPrefix}/plans`, (req, res) => {
  fs.readFile('./plans.csv', async (err, fileData) => {
    if (err) {
      res.status(500).send({ message: 'File parsed with error' })
    }
    parse(fileData, {columns: true, trim: true}, function(err, rows) {
      res.json(rows)
      // Your CSV data is in an array of arrys passed to this callback as rows.
    })
  })
});


app.get(`${ApiPrefix}/user/logout`, (req, res) => {
    res.clearCookie('token')
    res.status(200).end()
});

app.get(`/auth`, (req, res) => {
    res.redirect('https://sudotech.yufuid.com/sso/ai-3bb48d0516e7451eb8aa85c4b42bd1cb')
});
app.get(`/auth/sso`, (req, res) => {
    const { query } = req
    let token = query['id_token']
    try {
      jwt.verify(token, publicKey, { algorithm: 'RS256' })
      res.cookie('token', token,{
        maxAge: 900000,
        httpOnly: true,
      })
      res.redirect('/')
    } catch (err) {
      const response = {}
      response.success = false
      response.message = err
      res.json(response)
    }
});

app.get(`${ApiPrefix}/user`, (req, res) => {
    const cookie = req.headers.cookie || ''
    const cookies = qs.parse(cookie.replace(/\s/g, ''), { delimiter: ';' })
    let token = cookies.token
    const response = {}
    if (!token) {
      res.status(200).send({ message: 'Not Login' })
      return
    }
    try {
      const userItem = jwt.verify(token, publicKey, { algorithm: 'RS256' })
      response.success = true
      if (userItem.permissions) {
        userItem.permissions = JSON.parse(userItem.permissions)
      }
      response.user = userItem
    } catch (err) {
      response.success = false
      response.message = err
    }
    
    res.json(response)
});

app.use(express.static(path.join(__dirname, '../dist')));

app.listen(port, () => console.log(`App listening on port ${port}!`))